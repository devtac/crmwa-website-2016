<?php

/**
 * @package    : Joomla.Module.K2.Content
 * @subpackage : Ariva Template
 * @since	   : v1.00
 * @copyright  : Regular / Extended Themeforest
 * @author     : TDGR, http://pixeldrops.net
 */

defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

	<?php if(count($items)): ?>
    
    	<?php foreach ($items as $key=>$item):	?>
      
            <div class="<?php echo ($key%2) ? "odd" : "even"; ?> blogItem blog-parallax<?php echo $key; ?>" style="background:url(<?php echo $item->image; ?>);">
            
                <?php if($params->get('itemImage') && isset($item->image)): ?>
                
                    <a class="blogMask" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
    
						<span class="blogItemIcon">
    
							<?php if($params->get('itemTags') && count($item->tags)>0): ?>
    
								<?php foreach ($item->tags as $tag): ?>
                                        
									<?php 
                                        
										if (($tag->name) == "image")  { echo '<i class="fa fa-camera-retro"></i>'; } 
											
										elseif (($tag->name) == "carousel")  { echo '<i class="fa fa-camera-retro"></i>';  } 
											
										elseif (($tag->name) == "audio")  { echo '<i class="fa fa-headphones"></i>';  }
											
										elseif (($tag->name) == "quote")  { echo '<i class="fa fa-quote-right"></i>';  } 
											
										elseif (($tag->name) == "video")  { echo '<i class="fa fa-play"></i>';  } 
											
										elseif (($tag->name) == "link")  { echo '<i class="fa fa-link"></i>';  } 
											
										elseif (($tag->name) == "standard") { echo '<i class="fa fa-link"></i>';  } 
                                        
									?>
                                    
								<?php endforeach; ?>
                                
							<?php endif; ?>
          
						</span>
                        
						<?php if($params->get('itemTitle')): ?>
    
							<span class="moduleItemTitle"><?php echo $item->title; ?></span>
    
						<?php endif; ?>
                        
                        <span class="blogItemDetails">
                            
							<?php if($params->get('itemDateCreated')): ?>
        
                                <?php echo JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC')); ?>
        
                            <?php endif; ?>
                            
                            <?php if($params->get('itemAuthor')): ?>
                            
                            	/ <?php echo $item->author; ?>
                                
                            <?php endif; ?>
                            
                            <?php if($params->get('itemCategory')): ?>
      
	  							/ <?php echo $item->categoryname; ?>
      
	  						<?php endif; ?>
                        
                        </span>
                    
                    </a>
    
                <?php endif; ?>
                
            </div>
            
        <?php endforeach; ?>
    
    <?php endif; ?>

</div>
