// Ajax Portfolio Content

jQuery(document).on("click", "#portfolio a", function(e) {
	
	e.preventDefault(); 
	
	e.stopPropagation();

	var jQuerythis = jQuery(this),
	
		ajaxContent = jQuery("#ajax-content"),
		
		dynURL = jQuery(this).attr("href");	
	
	jQuery('#ajax-wrapper').css('left',0);
	
	ajaxContent.load(dynURL + " #tdref"), function() {
						
	}
				
	jQuery( document ).ajaxStop(function() {
		
		// pur here all the scripts you wqnt to run inside ajax
  							
		jQuery(".container").fitVids();
		
		jQuery('.portfolio-bg').parallax("50%", 0.2);
		
		var owlcslider = jQuery("#owl-cslider");
		
		owlcslider.owlCarousel({
		 
			nav : true,
				
			navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				
			scrollPerPage : false,
				
			loop:true,
				
			dots : false,
				
			items:1,
	
		})
		
	});
						
		
});

jQuery(document).on("click", ".portfolio-close", function(event) {
	
	var ajaxContent = jQuery("#ajax-content");

	event.preventDefault(); 
	
	event.stopPropagation();
	
	ajaxContent.empty();
	
	jQuery('#ajax-wrapper').css('left','-100%');
	
});

jQuery(document).on("click", ".pager li.next a, .pager li.previous a", function(event){
	
	var jQuerythis = jQuery(this),
	
		ajaxContent = jQuery("#ajax-content"),
		
		dynURL = jQuery(this).attr("href");
		
	event.preventDefault();
	
	event.stopPropagation();
	
	ajaxContent.load(dynURL + " #tdref"), function(){
						
	}
				
	jQuery( document ).ajaxStop(function() {
							
		jQuery(".container").fitVids();
		
		jQuery('.portfolio-bg').parallax("50%", 0.2);
		
		var owlcslider = jQuery("#owl-cslider");
		
		owlcslider.owlCarousel({
			 
			nav : true,
				
			navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
				
			scrollPerPage : false,
				
			loop:true,
				
			dots : false,
				
			items:1,
	
		})
						
	});
	
});


jQuery(document).ready(function () {
	
	// Preloader
	
	jQuery('#status').fadeOut();
	
	jQuery('#preloader').delay(350).fadeOut('slow');
	
	// Fixed #feature caption and height
	
	jQuery('#feature .slider-text').css('padding-top', jQuery(window).height()/2);
	
	jQuery('#feature').css('height',(jQuery(window).height()-0));
	
	
	// Scroll buttons
	
	jQuery('.scroll').click(function(event) {

		event.preventDefault();

		var target = "#" + jQuery(this).data('target');

		jQuery('html, body').animate({scrollTop:jQuery(target).offset().top}, 600);
		
		return false;

	})


	// Parallax Effect
	
	jQuery('.s1parallax').parallax("50%", 0.5);
	
	jQuery('.s2parallax').parallax("50%", 0.1);
	
	jQuery('.s3parallax').parallax("50%", 0.1);
	
	jQuery('.s4parallax').parallax("50%", 0.1);
	
	jQuery('.s5parallax').parallax("50%", 0.1);
	
	jQuery('.s6parallax').parallax("50%", 0.1);
	
	jQuery('.s7parallax').parallax("50%", 0.1);
	
	jQuery('.s8parallax').parallax("50%", 0.1);
	
	jQuery('.s9parallax').parallax("50%", 0.1);
	
	jQuery('.s10parallax').parallax("50%", 0.05);
	
	jQuery('.s11parallax').parallax("50%", 0.1);
	
	jQuery('.s12parallax').parallax("50%", 0.1);
	
	jQuery('.featureparallax').parallax("100%", 0.1);
	
	jQuery('.header-page').parallax("50%", 0.2);
	
	jQuery('.blog-parallax0').parallax("50%", 0.2);
	
	jQuery('.blog-parallax1').parallax("50%", 0.4);
	
	jQuery('.blog-parallax2').parallax("50%", 0.3);
	
	jQuery('.blog-parallax3').parallax("50%", 0.2);
	
	jQuery('.blog-parallax4').parallax("50%", 0.8);
	
	jQuery('.blog-parallax5').parallax("50%", 0.1);
	
	// Responsive Video
	
	jQuery(".container").fitVids();
	
	
	// Counter
	
	jQuery('.timer').countTo();
	
	
	// Owl rotators
	
	var owlteam = jQuery("#owl-team");
	
	var owlquote = jQuery("#owl-quote");
	
	var owlcslider = jQuery("#owl-cslider");
	
	var owltwitter = jQuery("#owl-twitter");
	
	var owlslider = jQuery("#owl-slider");
	
	var owlservices = jQuery("#owl-services");
	
	var owlbrands = jQuery("#owl-brand");
	
	var owlprocessleft = jQuery("#owl-process-left");
	
	var owlprocessright = jQuery("#owl-process-right");
	
	owlbrands.owlCarousel({
		
		nav : true,
		
		navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		
		scrollPerPage : false,
		
		loop:true,
		
		dots : false,
		
		responsiveClass:true,
		
		responsive : {
		
			1200 : {
				items : 5,
			},

			992 : {
				items: 4,
			},

			768 : {
				items:3,
			},
		
			0 : {
				items:2,
			}
	
		}
	
	});
	
	owlservices.owlCarousel({

		nav : false,
		
		autoplay : false,

		scrollPerPage : false,

		loop : true,

		dots : true,

		responsiveClass : true,
		
		margin : 30,
	
		responsive : {
			
			1200 : {
				items : 3,
			},
			
			992 : {
				items: 3,
			},
		
			768 : {
				items:2,
			},
			
			0 : {
				items:1,
			}
		}
	})
	
	owlslider.owlCarousel({

		items : 1, 

		scrollPerPage : false,

		loop:true,

		dots : false,

	});
	
	owltwitter.owlCarousel({
		
		nav : true,

		navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		
		scrollPerPage : false,

		loop:true,

		dots : false,

		items:1,

	})
	
	owlprocessleft.owlCarousel({
		
		nav : true,

		navText : ['<','>'],
		
		scrollPerPage : false,

		loop:true,

		dots : false,

		items:1,
		
		autoplay : true,

	})
	
	owlprocessright.owlCarousel({
		
		nav : true,

		navText : ['<','>'],
		
		scrollPerPage : false,

		loop:true,

		dots : false,

		items:1,
		
		autoplay : true,

	})
	
	owlcslider.owlCarousel({
		 
		nav : true,
			
		navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			
		scrollPerPage : false,
			
		loop:true,
			
		dots : false,
			
		items:1,

	})

	owlquote.owlCarousel({

		nav : false,
		
		scrollPerPage : false,

		loop:true,

		dots : true,

		items:1,
		
		autoplay : true,

	})
	
	owlteam.owlCarousel({
			
		nav : false,
			
		scrollPerPage : false,
			
		loop:true,
			
		dots : false,
			
		responsiveClass:true,
		
		margin:100,
			
		responsive : {
			
			1200 : {
				items : 3,
			},
				
			992 : {
				items: 3,
			},
				
			768 : {
				items:2,
			},
				
			0 : {
				items:1,
			}
				
		}
			
	})
	
	
	// Pricing tables fix

	jQuery( ".pr-style1 > tbody > tr:first > td" ).css( "padding-top", 35 );


	// One page menu script 
	
	if ( ! window.console ) console = { log: function(){} };
	
	jQuery('.subMenu').singlePageNav({
	
		offset: 80,
	
		filter: ':not(.external)',
	
		currentClass: 'actview',
	
		speed: 800 ,
	
	});


	// Menu toggle button
	
	jQuery( ".desktop-menu" ).unbind('click').click(function(event) {
		
		event.preventDefault(); 
	
		event.stopPropagation();
				
		jQuery('ul.nav').toggle( "slow" );
		
		jQuery('.social-nav').toggle( "slow" );
		
		return false;
	
	});
	
	
	// Mobile sidebar
	
	jQuery( ".mobile-menu" ).click(function(event) {
		
		event.preventDefault(event);
		
		event.stopPropagation(event);
		
	  jQuery( ".mobile-sidebar" ).animate({ right:0 }, 400 );
	  
	});
	
	jQuery( ".mobile-close" ).click(function(event) {
		
		event.preventDefault(event);
		
		event.stopPropagation(event);
		
	  jQuery( ".mobile-sidebar" ).animate({ right:-270 }, 400 );
	  
	});
	

});