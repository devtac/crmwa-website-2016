<?php

/**
 * @package    : Joomla
 * @subpackage : Ariva Template
 * @since	   : v1.2.3
 * @copyright  : Regular / Extended Themeforest
 * @author     : TDGR, http://pixeldrops.net
 */
 
defined('_JEXEC') or die;

?>

<!-- SITE TITLE OR LOGO / STICKY NAV -->
<?php if ($this->params->get('logoFileFixed')) {
	$logofixed = '<img alt="logo" style="width: 270px;" src="'. JURI::root() . $this->params->get('logoFileFixed') .'" />';
}
elseif ($this->params->get('sitetitleFixed')) {
	$logofixed = htmlspecialchars($this->params->get('sitetitleFixed'));
} ?>
    
<!-- TYPOGRAPHY -->
<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('BodyFontName');?>:<?php echo $this->params->get('BodyFontWeight');?>' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('MenuFontName');?>:<?php echo $this->params->get('MenuFontWeight');?>' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('HeadingFontName');?>:<?php echo $this->params->get('HeadingFontWeight');?>' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('TitleFontName');?>:<?php echo $this->params->get('TitleFontWeight');?>' rel='stylesheet' type='text/css' />

<style type="text/css">

	#status {
		background-image:url('<?php echo JURI::root() . $this->params->get('preloaderimg') ?>');
	}

	body, 
	.categories-module h4, 
	.sprocket-mosaic-tags li 
	{
		font-family: '<?php echo str_replace('+', ' ', $this->params->get('BodyFontName'));?>', sans-serif !important;
		font-size: <?php echo $this->params->get('BodyFontSize');?> !important;
		font-weight:<?php echo $this->params->get('BodyFontWeight');?> !important;
	}
	
	h1, h2, h3, h4, h5, h6, 
	.section-header, 
	.quote-author, 
	.timer-title, .timer, 
	.pr-plan, a.followme, 
	.process-title, 
	.postText, 
	.portfolio-mask-title,
	.button, 
	.sp-tweet-item, 
	.sp-tweet .date a, 
	#copyright, 
	#royal_preloader.royal_preloader_logo .percentage, 
	.catItemHeader,
	.itemHeader,
	.tagItemHeader,
	.userItemHeader,
	.progress-label,
	.progress-percentage,
	.blogItemDetails,
	.folio-title,
	.folio-date,
	.commentAuthorName,
	.commentAuthorName a  {
		font-family: '<?php echo str_replace('+', ' ', $this->params->get('HeadingFontName'));?>', sans-serif !important;
		font-weight: <?php echo $this->params->get('HeadingFontWeight');?> !important;
	}
	
	.nav a, 
	.nav span {
		font-family: '<?php echo str_replace('+', ' ', $this->params->get('MenuFontName'));?>', sans-serif !important;
		font-size: <?php echo $this->params->get('MenuFontSize');?> !important;
		font-weight: <?php echo $this->params->get('MenuFontWeight');?> !important;
	}
	
	.site-title{
		font-family: '<?php echo str_replace('+', ' ', $this->params->get('TitleFontName'));?>', sans-serif !important;
		font-size: <?php echo $this->params->get('TitleFontSize');?> !important;
		font-weight: <?php echo $this->params->get('TitleFontWeight');?> !important;
	}	
	
</style>


<!-- OPTIONS LOOP START -->
<?php for( $i=1; $i<13; $i++ ) { ?>

<style type="text/css">
	.s<?php echo $i ?>parallax,
	.s<?php echo $i ?>parallax .quote-message,
	.s<?php echo $i ?>parallax .quote-author,
	.s<?php echo $i ?>parallax .quote-job {
		color:#ffffff;
	}
	
	.s<?php echo $i ?>parallax .td-quote {
		background:#ffffff;
		color:#181818;
	}
	
</style>

<?php if (($this->params->get('s'.$i.'bgtype') == "1") or ($this->params->get('s'.$i.'bgtype') == "6")) { ?>
<style type="text/css">
	.section<?php echo $i ?> { 
		background: url(<?php echo JURI::root() . $this->params->get('s'.$i.'image') ?>);
		position:relative;
		background-position: center center;
		background-size: cover;
	}
</style>
<?php } ?>

<?php if ($this->params->get('s'.$i.'bgtype') == "2") { ?>
<style type="text/css">
	.section<?php echo $i ?> {
		background: url(<?php echo JURI::root() . $this->params->get('s'.$i.'image') ?>);
		background-attachment:fixed !important;
		background-size:cover;
		background-position:center center;
		position:relative;
	}
	@media (max-width: 768px) {
		.section<?php echo $i ?> {
			background: url(<?php echo JURI::root() . $this->params->get('s'.$i.'image') ?>) !important;
			position:relative;
			overflow:hidden;
			background-position:center !important;
			background-size:cover !important;
			background-attachment:fixed !important;
		}
	}
</style>
<?php } ?>

<?php if ($this->params->get('s'.$i.'bgtype') == "3") { ?>
<style type="text/css">
	.section<?php echo $i ?> {
		background:none;
		position:relative;
		overflow:hidden;
		background-repeat:no-repeat;
	}
	@media (max-width: 768px) {
		.section<?php echo $i ?> {
			background: url(<?php echo JURI::root() . $this->params->get('s'.$i.'image') ?>) !important;
			position:relative;
			overflow:hidden;
			background-position:center !important;
			background-size:cover !important;
			background-attachment:fixed !important;
		}
	}
</style>
<?php } ?>

<?php if ($this->params->get('s'.$i.'bgtype') == "4") { ?>
<style type="text/css">
	.section<?php echo $i ?> {
		background: url(<?php echo JURI::root() . $this->params->get('s'.$i.'image') ?>);
		position: relative;
		background-repeat: repeat;
	}
</style>
<?php } ?>


<?php if ($this->params->get('s'.$i.'bgtype') == "5") : ?>
	<?php if ($this->params->get('s'.$i.'color') == "2") { ?>
		<style type="text/css">
            .section<?php echo $i ?> {
                background: #fff !important;
                position: relative;
                z-index:50;
            }
        </style>
	<?php } ?>
    <?php if ($this->params->get('s'.$i.'color') == "3") { ?>
		<style type="text/css">
            .section<?php echo $i ?> {
                background: #f0f0f0 !important;
                position: relative;
                z-index:50;
            }
        </style>
	<?php } ?>
<?php endif ?>

<style type="text/css">
	.section<?php echo $i ?>  
	{
		padding:<?php echo $this->params->get('s'.$i.'padding') ?>;
	}
</style>

<?php } ?>

<?php if (($this->params->get('featurebgtype') == "1") or ($this->params->get('featurebgtype') == "6")) { ?>
<style type="text/css">
	#feature { 
		background: url(<?php echo JURI::root() . $this->params->get('featureimage') ?>);
		position:relative;
		z-index:50;
		background-position: center center;
		background-size: cover;
		padding:0px;
	}
</style>
<?php } ?>

<?php if ($this->params->get('featurebgtype') == "2") { ?>
<style type="text/css">
	#feature {
		background: url(<?php echo JURI::root() . $this->params->get('featureimage') ?>);
		z-index:50;
		background-size: cover;
		-webkit-background-size:cover;
		-moz-background-size:cover;
		background-attachment: fixed;
		padding:0px;
	}
	@media (max-width: 768px) {
		#feature {
		background: url(<?php echo JURI::root() . $this->params->get('featureimage') ?>) !important;
		background-position: center center !important;
		background-size: cover;
		-webkit-background-size:cover;
		-moz-background-size:cover;
		max-width:100%;
		padding:0px;
		position:relative;
		z-index:50;
		overflow:hidden;
	}
	}
</style>
<?php } ?>

<?php if ($this->params->get('featurebgtype') == "3") { ?>
<style type="text/css">
	#feature {
		background:none;
		padding:0px;
		position:relative;
		z-index:50;
		overflow:hidden;
	}
	@media (max-width: 768px) {
		#feature {
		background: url(<?php echo JURI::root() . $this->params->get('featureimage') ?>) !important;
		background-position: center center !important;
		background-size: cover;
		-webkit-background-size:cover;
		-moz-background-size:cover;
		max-width:100%;
		padding:0px;
		position:relative;
		z-index:50;
		overflow:hidden;
	}
	}
</style>
<?php } ?>

<?php if ($this->params->get('featurebgtype') == "4") { ?>
<style type="text/css">
	#feature {
		background: url(<?php echo JURI::root() . $this->params->get('featureimage') ?>);
		position: relative;
		z-index:50;
		background-repeat: repeat;
		padding:0px;
	}
</style>
<?php } ?>

<?php if ($this->params->get('featurebgtype') == "5") : ?>
	<?php if ($this->params->get('featurecolor') == "1") { ?>
		<style type="text/css">
			#feature h1, #feature h2, #feature h3, #feature h4, #feature h5, #feature h6, #feature .section-header { color:#222222 !important; }
            #feature {
                background: #ffffff;
                position: relative;
                padding:0px;
				z-index:50;
            }
        </style>
	<?php } ?>
    <?php if ($this->params->get('featurecolor') == "2") { ?>
    	<style type="text/css">
            #feature {
                background:<?php echo $this->params->get('themecolor') ?>;
                position: relative;
                z-index:50;
                padding:0px;
            }
        </style>
	<?php } ?>
    <?php if ($this->params->get('featurecolor') == "3") { ?>
    	<style type="text/css">
            #feature {
                background: #f0f0f0;
                position: relative;
                z-index:50;
                padding:0px;
            }
        </style>
	<?php } ?>
<?php endif ?>


<!-- THEME COLOR CSS -->
<style type="text/css"> 

.nav > li.active > a, 
.nav > li.active > a:hover, 
.nav > li.active > a:focus, 
.nav li.current a,
.nav .open>a,
.nav .open>a:hover,
.nav .open>a:focus,
.footer-opacity i,
.nav a:hover, 
a:hover, 
ul.topmenu li a:hover, 
.pagination ul li:hover a, 
.pagination ul li:hover span, 
.tags a,
.dropdown-menu >li> a:hover,
div.k2CategoriesListBlock ul li:hover:before,
div.k2ItemsBlock ul li:hover:before,
.btn-style2,
#ajax-content .pager .next>a:hover,
#ajax-content .pager .previous>a:hover,
.moduletable-portfolio .portfolio-close:hover,
.portfolio-close:hover i
{ 
	color:<?php echo $this->params->get('themecolor') ?>;
}

.article-info a:hover, 
.label-info:hover, 
#nav ul.nav li:hover a,
.filter-search-lbl, 
.actview, 
.commentLink a,
.td-portfolio-mosaic-order ul li:hover, 
.td-portfolio-mosaic-filter ul li:hover,
.td-portfolio-mosaic-filter ul li.active,
.td-portfolio-mosaic-loadmore:hover,
.process:hover .process-title,
.process:hover i,
.blog .moduleItemComments:hover,
.blog-item:hover .moduleItemTitle,
.k2CategoriesListBlock ul li:hover a,
.k2ItemsBlock ul li:hover a,
.catItemTitle a:hover,
.tagItemTitle a:hover,
.contact-email,
 #copyright i.social-copy:hover,
 .btn-style1:hover,
 .member-mask a i:hover
{ 
	color:<?php echo $this->params->get('themecolor') ?> !important; 
}

.moduletable-style2, 
span.tag:hover a, 
.tags a:hover,table.pr-style1 > thead.offer > tr > th,  
.k2TagCloudBlock a:hover, 
.progress-bar, 
.label-info, 
.badge, 
.tp-bullets.simplebullets.round .selected, 
.btn-services:hover, 
.horizontal-line,
.sp-tweet i,
.btn-style2:hover,
.btn-style3:hover,
.itemIcon,
#owl-cslider .owl-prev:hover, 
#owl-cslider .owl-next:hover,
.offer .pr-icon,
#owl-process-right .owl-prev:hover,
#owl-process-left .owl-prev:hover,
#owl-process-right .owl-next:hover,
#owl-process-left .owl-next:hover,
.tparrows.default:hover::after
{
	background-color:<?php echo $this->params->get('themecolor') ?> !important;
}

.label-info:hover,
.k2TagCloudBlock a:hover,
.tags a, 
.td-portfolio-mosaic-loadmore,
.td-portfolio-mosaic-filter ul li.active,
.td-portfolio-mosaic-order ul li:hover, 
.td-portfolio-mosaic-filter ul li:hover
{ 
	border:<?php echo $this->params->get('themecolor') ?> 1px solid !important;
}

a.a-item:hover > .mask-1,
.blogItem:hover .blogMask
{
	background-color:<?php echo $this->params->get('themecolor') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-2
{
	background-color:<?php echo $this->params->get('themecolor2') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-3
{
	background-color:<?php echo $this->params->get('themecolor3') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-4
{
	background-color:<?php echo $this->params->get('themecolor4') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-5
{
	background-color:<?php echo $this->params->get('themecolor5') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-6
{
	background-color:<?php echo $this->params->get('themecolor6') ?> !important; opacity:.9;
}

a.a-item:hover > .mask-7
{
	background-color:<?php echo $this->params->get('themecolor7') ?> !important; opacity:.9;
}
a.a-item:hover > .mask-0
{
	background-color:<?php echo $this->params->get('themecolor8') ?> !important; opacity:.9;
}


.tp-bullets.simplebullets.round .selected,
.btn-style1:hover,
.btn-style2:hover,
.btn-style2,
.btn-style3:hover,
.btn-style1,
 #copyright i.social-copy:hover
{ 
	border:<?php echo $this->params->get('themecolor') ?> 2px solid !important;
}

.td-portfolio-mosaic-loadmore,
.btn-style1,
.member-icon,
.service:hover,
.service-line,
.td-quote
{
	background-color:<?php echo $this->params->get('themecolor') ?>; 
}

.td-portfolio-mosaic-loadmore:hover 
{
	background-color:transparent;
}

#nav ul.nav li:hover a,
.actview  {
	border-bottom: 1px solid <?php echo $this->params->get('themecolor') ?>;
}

.map iframe {
	margin-bottom:-7px;
}

#map {
	overflow:hidden;
	height:<?php echo $this->params->get('mapheight') ?>;
}

a {
	text-decoration:none !important;
}

</style>
	
<!--other pages-->
<?php if ($menu->getActive() != $menu->getDefault($lang->getTag())) : ?>
	<style type="text/css">
	
    #map {
		display:none;
	}
       
    </style>
<?php endif; ?>